import {BrowserModule} from '@angular/platform-browser';
import {ErrorHandler, NgModule} from '@angular/core';
import {AppComponent} from './app.component';
import {FileUploaderComponent} from './uploader/file-uploader.component';
import {UploadInterceptor} from './uploader/mock/upload-interceptor.service';
import {HTTP_INTERCEPTORS, HttpClientModule} from '@angular/common/http';
import {FileUploaderService} from './uploader/file-uploader.service';
import {UploadProgressBarComponent} from "./uploader/upload-progress-bar.component";
import {GlobalErrorHandler} from './uploader/error-handler/global-error-handler.service';


@NgModule({
  declarations: [
    AppComponent,
    FileUploaderComponent,
    UploadProgressBarComponent
  ],
  imports: [
    BrowserModule,
    HttpClientModule
  ],
  providers: [
    {provide: HTTP_INTERCEPTORS, useClass: UploadInterceptor, multi: true},
    {provide: ErrorHandler, useClass: GlobalErrorHandler},
    FileUploaderService,
  ],
  bootstrap: [AppComponent]
})
export class AppModule {
}
