export class CustomThrowable extends Error {
  public type: string;

  constructor(type: string, message?: string) {
    super(message);
    this.type = type;
  }
}

export class ApplicationError extends CustomThrowable {
  constructor(message?: string) {
    super('Error', message);
  }
}

export class ApplicationWarning extends CustomThrowable {
  constructor(message?: string) {
    super('Warning', message);
  }
}

export class ApplicationInfo extends CustomThrowable {
  constructor(message?: string) {
    super('Info', message);
  }
}
