import {HttpClient} from '@angular/common/http';
import {ErrorHandler, Injectable} from '@angular/core';
import {LogEntry} from './log-entry.model';
import {environment} from '../../../environments/environment';

@Injectable()
export class GlobalErrorHandler implements ErrorHandler {

  constructor(private http: HttpClient) {
  }

  handleError(error: any): void {
    if (environment.isDebugMode) {
      const errorType = this.determineErrorClass(error);
      let logEntry = this.buildLogEntry(error, errorType);
      if (errorType === 'Error') {
        this.http.post('/', logEntry).subscribe(() => console.log('Error sent to server'));
      } else {
        console.error(error);
      }
    }
  }

  private buildLogEntry(error: any, errorType: string): LogEntry {
    return <LogEntry> {
      pageTitle: document.title,
      errorMessage: error.message,
      errorType: errorType,
      errorStack: error.stack,
      errorSource: error.ngDebugContext.component.constructor.name,
      browserData: window.navigator.userAgent
    };
  }

  private determineErrorClass(error: any) {
    if (error.type !== undefined) {
      return error.type;
    } else {
      return 'Error';
    }
  }
}
