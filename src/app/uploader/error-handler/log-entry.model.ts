export interface LogEntry {
  pageTitle: string;
  errorType: string;
  errorStack: string;
  errorMessage: string;
  errorSource: string;
  serverVariables: string;
  browserData: string;
}
