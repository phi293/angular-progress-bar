import {Component} from '@angular/core';
import {FileUploaderService} from './file-uploader.service';
import {HttpEvent} from "@angular/common/http";
import {HttpEventType} from "@angular/common/http";
import {HttpResponse} from "@angular/common/http";
import {Observable} from "rxjs/Observable";
import "rxjs/add/operator/filter";
import "rxjs/add/operator/map";
import {ApplicationError, ApplicationInfo, ApplicationWarning} from './error-handler/custom-throwables';

@Component({
  selector: 'file-uploader',
  templateUrl: './file-uploader.component.html'
})
export class FileUploaderComponent {

  protected uploadedFiles: FileList;

  constructor() {
  }

  uploadFiles() {
    // this.uploadedFiles.length //application error because uploadedFile is undefined
    // throw new ApplicationError('this is application error which will be sent to server');
    // throw new ApplicationWarning('this is application error class warning which will not be sent to server');
    throw new ApplicationInfo('this is application error class info which will not be sent to server');
  }
}
