import {HttpClient, HttpEvent, HttpRequest} from '@angular/common/http';
import {Observable} from 'rxjs/Observable';
import {Injectable} from '@angular/core';
import "rxjs/add/operator/share";

@Injectable()
export class FileUploaderService {

  constructor(private http: HttpClient) {
  }

  uploadFiles(files: File[]): Observable<HttpEvent<any>> {
    const formData: FormData = new FormData();
    for (let i = 0; i < files.length; i++) {
      formData.append('uploadedFiles', files[i], files[i].name);
    }
    const req = new HttpRequest('POST', '/upload/file', formData, {reportProgress: true});
    return this.http.request<any>(req).share();
  }
}
