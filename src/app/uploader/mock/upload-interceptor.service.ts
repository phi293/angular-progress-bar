import {Injectable} from '@angular/core';
import {HttpEvent, HttpHandler, HttpInterceptor, HttpRequest, HttpResponse} from '@angular/common/http';
import {Observable} from 'rxjs/Observable';

@Injectable()
export class UploadInterceptor implements HttpInterceptor {

  intercept(req: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
    return new Observable<HttpEvent<any>>(observer => {
      const doneResponse = new HttpResponse({
        status: 201,
      });
      console.log('json sent to server: ' + JSON.stringify(req.body));
      observer.next(doneResponse);
      observer.complete();
      return;
    });
  }
}
