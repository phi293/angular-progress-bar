import {Component} from "@angular/core";
import {Input} from "@angular/core";
import {OnChanges} from "@angular/core";
import {SimpleChanges} from "@angular/core";
import {OnInit} from "@angular/core";
import {Observable} from "rxjs/Observable";
import {HttpEvent} from "@angular/common/http";
import {HttpEventType} from "@angular/common/http";
import "rxjs/add/operator/filter";

@Component({
  selector: 'upload-progress-bar',
  templateUrl: './upload-progress-bar.component.html'
})
export class UploadProgressBarComponent implements OnInit, OnChanges {

  @Input() private progressObservable: Observable<HttpEvent<any>>;
  protected currentPercentage: number;

  ngOnInit(): void {
    this.currentPercentage = 0;
  }

  ngOnChanges(changes: SimpleChanges): void {
    if (changes && changes.progressObservable.currentValue) {
      this.progressObservable.subscribe(event => {
        this.changeProgressBarFilling(event);
      });
    } else {
      this.currentPercentage = 0;
    }
  }

  private changeProgressBarFilling(event) {
    switch (event.type) {
      case HttpEventType.UploadProgress:
        let progress = Math.round(100 * event.loaded / event.total);
        this.currentPercentage = progress;
        break;
      case HttpEventType.Response: {
        this.currentPercentage = 100;
      }
    }
  }
}
